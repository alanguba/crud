<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * Users mailer.
 */
class UsersMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'Users';

    public function welcome($user,$pass){
    	$this->to($user->email)
    	->profile('alangb')
    	->emailFormat('html')
        ->template('alangb_message')
    	->layout('users')
    	->viewVars(['name'=> $user->nombre, 'cont'=>$pass])
    	->subject(sprintf('Bienvenido, %s',$user->nombre));

    }

    public function retrieve($email,$id,$nombre){
        $this->to($email)
        ->profile('alangb')
        ->emailFormat('html')
        ->template('alangb_recuperar')
        ->layout('users')
        ->viewVars(['correo'=> $email,'id'=>$id,'nombre'=>$nombre])
        ->subject(sprintf('Recuperación de tu contraseña'));

    }
}