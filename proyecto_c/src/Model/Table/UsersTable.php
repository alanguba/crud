<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Proffer.Proffer', [
            'photo' => [    // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'photo_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    /*'square' => [   // Define the prefix of your thumbnail
                        'w' => 200, // Width
                        'h' => 200, // Height
                        'crop'=> true,
                        'jpeg_quality'  => 100
                    ]*/
                ],
                'thumbnailMethod' => 'gd'   // Options are Imagick or Gd
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->decimal('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create','Falta este campo.')
            ->allowEmptyString('email', false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmptyString('password');

        $validator
            ->scalar('nombre')
            ->minLength('nombre',3, 'Debe tener 3 digitos como mínimo.')
            ->maxLength('nombre', 255, 'El nombre no puede ser tan largo.')
            ->requirePresence('nombre', 'create','Es necesario este campo.')
            ->allowEmptyString('nombre', false)
            ->add('nombre', 'validFormat',[
                'rule' => array('custom', '/^[A-z\sáéíúóñÑ]*$/i'),
                'message' => 'El nombre no puede tener números'
        ]);

        $validator
            ->scalar('apellidop')
            ->maxLength('apellidop', 255,'El apellido no puede ser tan largo.')
            ->requirePresence('apellidop', 'create','Es necesario este campo.')
            ->allowEmptyString('apellidop', false)
            ->add('apellidop', 'validFormat',[
                'rule' => array('custom', '/^[A-z\sáéíúóñÑ]*$/i'),
                'message' => 'El apellido no puede tener números'
        ]);

        $validator
            ->scalar('apellidom')
            ->maxLength('apellidom', 255,'El apellido no puede ser tan largo.')
            ->allowEmptyString('apellidom');


        $validator
            ->scalar('estado')
            ->maxLength('estado', 20)
            ->requirePresence('estado', 'create','Es necesario este campo.')
            ->allowEmptyString('estado', false);

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 20)
            ->requirePresence('tipo', 'create','Es necesario este campo.')
            ->allowEmptyString('tipo', false);

        $validator
            ->provider('proffer', 'Proffer\Model\Validation\ProfferRules')
            ->add('photo', 'extension', [
                'rule' => ['extension', ['gif','png','jpg']],
                'message' => 'La imagen debe ser tipo jpeg, png o jpg.',
            ])
            ->add('photo','fileSize',[
                'rule' => ['fileSize','<=','1MB'],
                'message' => 'La imagen debe ser menor o igual a 1MB.',
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'],'Este correo ya está registrado.'));
        return $rules;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        
        $query
            ->select(['id','email', 'password','tipo'])
            ->where(['Users.estado' => 'activo']);
        return $query;
    }

    function password_ram($long=10){
        $charset="abcdefghijklmnopqrstuvwxyz0123456789?!#";
        $password="";
        for($i=0;$i<$long;$i++){
            $rand= rand() % strlen($charset);
            $password.=substr($charset,$rand,1);
        }
        return $password;

    }

    function passCheck($pass){
        if (!preg_match('/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{10,}$/i',$pass))
        {
         return false;
        }
        else
        {
         return true;
        }
    }

}
