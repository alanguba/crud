<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher; 

/**
 * User Entity
 *
 * @property float $id
 * @property string $email
 * @property string|null $password
 * @property string $nombre
 * @property string $apellidop
 * @property string|null $apellidom
 * @property string $estado
 * @property string $tipo
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'nombre' => true,
        'apellidop' => true,
        'apellidom' => true,
        'estado' => true,
        'tipo' => true,
        'photo'=>true,
        'photo_dir'=> false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($value);
        }
    }
}
