<?php
namespace App\Controller;


use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Users.apellidop' => 'asc'
        ]
    ];

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,     // true/false
            'sitekey' => '6Lc87IoUAAAAAPPpwkUxa_EwD-aGk1gJGsQBhdq2', //if you don't have, get one: https://www.google.com/recaptcha/intro/index.html
            'secret' => '6Lc87IoUAAAAAMaVJCw6GvfqCtflKK-VrKZu8X3n',
            'type' => 'image',  // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'es',      // default en
            'size' => 'normal'  // normal/compact
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }*/

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    use MailerAwareTrait;
    public function isAuthorized($user)
    {
        // All registered users can add articles
        if (in_array($this->request->getParam('action'), ['change', 'logout','index'])) {
            return true;
        }
        return parent::isAuthorized($user);
    }

    public function add()
    {
        $bitacoraTable=TableRegistry::get( 'bitacora' );
        $usuarioTable=TableRegistry::get( 'users' );
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $pass=$usuarioTable->password_ram();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->password=$pass;
            $this->getMailer('Users')->send('welcome',[$user,$pass]);
            if ($this->Users->save($user)) {
                $bitacoraTable->registro('Agregó a un nuevo usuario.',$this->Auth->user('id'));
                $this->Flash->success(__('Se agregó el usuario correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo agregar al usuario.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bitacoraTable=TableRegistry::get( 'bitacora' );
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $bitacoraTable->registro('Actualizó a un usuario.',$this->Auth->user('id'));
                $this->Flash->success(__('El usuario se atualizó correctamente'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no se pudo actualizar'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $bitacoraTable=TableRegistry::get( 'bitacora' );
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $bitacoraTable->registro('Eliminó a un usuario.',$this->Auth->user('id'));
            $this->Flash->success(__('El usuario ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El usuario no se pudo eliminar.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login() {
        
        if($this->request->is('post')){
            $user=$this->Auth->identify();
            if($this->Recaptcha->verify()){
                if($user){
                    $this->Auth->setUser($user);
                    $bitacoraTable=TableRegistry::get( 'bitacora' );
                    $bitacoraTable->registro('Inició sesión.',$this->Auth->user('id'));
                    return $this->redirect(['controller'=>'users','action'=>'index']);
                }
                $this->Flash->error('El email o contraseña son incorrectos.');
            }else{
                $this->Flash->error('Verifica el Recaptcha.');
            } 
        }
    }

    public function logout()
    {
        $bitacoraTable=TableRegistry::get( 'bitacora' );
        $bitacoraTable->registro('Cerró sesión.',$this->Auth->user('id'));
        return $this->redirect($this->Auth->logout());
    }

    public function recuperar(){
        if($this->request->is('post')){
            $correo=$this->request->getData('email');
            $sql=$this->Users->find()->where(['email'  =>  "$correo"]);
            foreach  ( $sql  as  $usuario )  { 
                $this->getMailer('Users')->send('retrieve',[$usuario->email,$usuario->id,$usuario->nombre]);
                $this->Flash->success(__('Se ha enviado un email a su correo con un link para recuperar la contraseña.'));
            }
        }
    }

    public function forgot($token=null){
        $usuarioTable=TableRegistry::get( 'users' );
        $bitacoraTable=TableRegistry::get( 'bitacora' );
        if($token!= null){
            $token=substr($token,2);
            $token=substr($token,0,-2);
        }
        $user = $usuarioTable->get($token);
        $passw=$usuarioTable->password_ram();
        $user->password=$passw;
        $bitacoraTable->registro('Restableció su contraseña.',$token);
        if($usuarioTable->save($user)){
            $this->set(compact('passw'));
            $this->Flash->success(__('Tu contraseña ha sido actualizada.'));
        }
    }

    public function change(){
        
        if($this->request->is('post')){
            $id=$this->Auth->user('id');
            $usuarioTable=TableRegistry::get( 'users' );
            $bitacoraTable=TableRegistry::get( 'bitacora' );
            $user = $usuarioTable->get($id);
            $password=$user->password;
            $contrasenas=$this->request->getData();
            $cont_nueva=$contrasenas['password'];
            if($usuarioTable->passCheck($cont_nueva)){
                if($contrasenas['password']== $contrasenas['password2']){
                    if(password_verify($contrasenas['passOld'], $password)){
                        $user->password=$contrasenas['password'];
                        if($usuarioTable->save($user)){
                            $bitacoraTable->registro('Cambió la contraseña',$this->Auth->user('id'));
                            $this->Flash->success(__('Tu contraseña ha sido actualizada.'));

                            return $this->redirect(['action' => 'index']);
                        }
                    }
                    else{
                        $this->Flash->error('La contraseña actual es incorrecta.');
                     }
                }
                else{
                    $this->Flash->error('Las contraseñas no coinciden');
                }
            }else{
                $this->Flash->error('La contraseña nueva debe tener una longitud mayor a 10, debe tener numeros y uno o más caracteres especiales.');
            }
        }
    }
}
