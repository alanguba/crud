<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div ><?= $this->Html->link(__('Nuevo usuario'), ['action' => 'add'],['class'=>'btn btn-outline-success']) ?></div>
<div ><?= $this->Html->link(__('Cambiar contraseña'), ['action' => 'change'],['class'=>'btn btn-outline-warning']) ?></div>

<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Usuarios') ?></h3>
    <table class="table table-striped table-hover" cellpadding="0" cellspacing="0">
        <thead>
            <tr> 
                <th scope="col"><?= $this->Paginator->sort('apellidop','Apellido Paterno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apellidom','Apellido Materno') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col">Tipo</th>
                <th scope="col">Estado</th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>

                
                <td><?= h($user->apellidop) ?></td>
                <td><?= h($user->apellidom) ?></td>
                <td><?= h($user->nombre) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->tipo) ?></td>
                <td><?= h($user->estado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id],['class'=>'btn btn-outline-success col-sm-12']) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id],['class'=>'btn btn-outline-danger col-sm-12','confirm' => __('Está seguro que desea eliminar al usuario {0}?', $user->nombre)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('primero')) ?>
            <?= $this->Paginator->prev('< ' . __('anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} elemento(s) de {{count}}')]) ?></p>
    </div>
</div>
