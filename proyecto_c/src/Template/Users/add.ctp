<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

function password_ram($long=10){
    $charset="abcdefghijklmnopqrstuvwxyz0123456789?!#";
    $password="";
    for($i=0;$i<$long;$i++){
        $rand= rand() % strlen($charset);
        $password.=substr($charset,$rand,1);
    }
    return $password;

}
?>

<div><?= $this->Html->link(__('Regresar'), ['action' => 'index'],['class'=>'btn btn-outline-info']) ?></div>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user,['type'=>'file']) ?>
    <fieldset>
        <legend><?= __('Agrega usuario') ?></legend>
        <?php
            echo $this->Form->control('email',['type' => 'email','label'=>'Correo']);
            echo $this->Form->control('nombre',['label'=>'Nombre(s)']);
            echo $this->Form->control('apellidop', ['label'=>'Apellido Paterno']);
            echo $this->Form->control('apellidom',['label'=>'Apellido Materno']);
            echo $this->Form->select('tipo',['administrador'=>'administrador', 'usuario'=>'usuario'],['empty' => 'Elige un tipo de usuario'],['name'=>'tipo']);
            echo $this->Form->select('estado',['activo'=>'activo', 'inactivo'=>'inactivo'],['empty' => 'Estado del usuario'],['name'=>'estado']);
            echo $this->Form->input('photo',['type'=>'file','label'=>'Foto de perfil']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Registrar'),['class'=>'btn btn-outline-success']) ?>
    <?= $this->Form->end() ?>
</div>
