<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend>
            <?php echo __('Inicio de Sesión'); ?>
        </legend>
        <?php echo $this->Form->input('email');
        echo $this->Form->input('password', array('type'=>'password'));
        echo $this->Recaptcha->display();
        echo $this->Form->submit('Entrar', array('class'=>'btn btn-success'));

    ?>
    </fieldset>
<?php echo $this->Form->end(); ?>
<div ><?= $this->Html->link(__('¿Olividó su contraseña?'), ['action' => 'recuperar']) ?></div>
</div>