<?php echo $this->Form->create(); ?>
<fieldset>
        <legend>
            <?php echo __('Cambiar de contraseña'); ?>
        </legend>
        <?php echo $this->Form->input('passOld', ['label'=>'Contraseña actual','type'=>'password']);
        echo $this->Form->input('password',['label'=>'Contraseña nueva','type'=>'password']);
        echo $this->Form->input('password2',['label'=>'Confirma la contraseña nueva','type'=>'password']);
        echo $this->Form->submit('Guardar cambios', array('class'=>'btn btn-success'));
    ?>
    </fieldset>
<?php echo $this->Form->end(); 
?>