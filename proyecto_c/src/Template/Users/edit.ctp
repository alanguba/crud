<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div><?= $this->Html->link(__('Regresar'), ['action' => 'index'],['class'=>'btn btn-outline-info']) ?></div>
<div><?= $this->Html->image('/webroot/files/users/photo/'. $user->photo_dir.'/'. $user->photo,['style'=>'width:300px; height:300px ','class'=>'img-thumbnail rounded float-left']); ?></div>
<div class="users form large-9 medium-8 columns content"> 
    <?= $this->Form->create($user,['type'=>'file']) ?>
    <fieldset>
        <legend><?= __('Edita un usuario') ?></legend>
        <?php
            echo $this->Form->control('email',['type' => 'email','label'=>'Correo','disabled']);
            echo $this->Form->control('nombre',['label'=>'Nombre(s)']);
            echo $this->Form->control('apellidop', ['label'=>'Apellido Paterno']);
            echo $this->Form->control('apellidom',['label'=>'Apellido Materno']);
            echo $this->Form->select('tipo',['administrador'=>'administrador', 'usuario'=>'usuario'],['empty' => 'Elige un tipo de usuario'],['name'=>'tipo']);
            echo $this->Form->select('estado',['activo'=>'activo', 'inactivo'=>'inactivo'],['empty' => 'Estado del usuario'],['name'=>'estado']);
            echo $this->Form->input('photo',['type'=>'file','label'=>'Foto de perfil']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Editar')) ?>
    <?= $this->Form->end() ?>
</div>
