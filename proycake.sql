CREATE TABLE users (
    id numeric(5,0) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255),
    nombre VARCHAR(255) NOT NULL,
    apellidoP VARCHAR(255) NOT NULL,
    apellidoM VARCHAR(255),
    estado VARCHAR(20) NOT NULL,
    tipo VARCHAR(20) NOT NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE bitacora (
    id numeric(5,0) NOT NULL,
    id_user numeric(5,0) NOT NULL,
    descripcion VARCHAR(255) NOT NULL,
    fecha TIMESTAMP NOT NULL,
    CONSTRAINT pk_bitacora PRIMARY KEY (id)
);

CREATE SEQUENCE users_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

CREATE SEQUENCE bitc_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE users ALTER COLUMN id SET DEFAULT nextval('users_seq');

ALTER TABLE bitacora ALTER COLUMN id SET DEFAULT nextval('bitc_seq');


INSERT INTO users (email, password, nombre, apellidoP,apellidoM,estado,tipo)
VALUES
('alan@correo.com', 'hola', 'Alan', 'Gutierrez','Bañuelos','activo','administrador');

ALTER TABLE users
ADD COLUMN photo VARCHAR(255) NULL,
ADD COLUMN photo_dir VARCHAR(255) NULL;